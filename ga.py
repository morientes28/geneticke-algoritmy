#!/usr/bin/env python
from bitarray import bitarray
import math
import random

# test data
#list_population.append(bitarray('00000001'))
#list_population.append(bitarray('01000010'))
#list_population.append(bitarray('01000011'))
#list_population.append(bitarray('01000111'))
#list_population.append(bitarray('01010111'))
#list_population.append(bitarray('01001111'))
#list_population.append(bitarray('01100111'))
#list_population.append(bitarray('01010101'))
#list_population.append(bitarray('01110111'))
#list_population.append(bitarray('11001111'))

""" Generovanie nahodneho chromozonu """
def generate_chromozone(interval):
	chromo = ""
	for i in range(0, int(interval)):
		gen = '1' if random.randint(0,1)==1 else '0'
		chromo += gen
	return chromo

""" Prevod binarneho cisla na integer """
def bit_to_dec(chromo):
	decimal = 0
	if chromo is None:
		return 0
	for digit in chromo:
		if isinstance(digit, int):
			decimal = decimal*2 + int(digit)
	return decimal	

""" ziskanie fitnes chromozomu, pricom vysledna funkcia je cosinus """
def get_fitness(chromo):
	#x = math.cos(bit_to_dec(chromo))
	return chromo

""" zobrazenie generacie chromozomov """
def show(list_population):
	if not debug:
		return
	for i in list_population:
		if isinstance(i, bitarray):
			print('x='+str(bit_to_dec(i)) + ' f(y)=' + str(get_fitness(i)) + ' chromozone='+ i.to01() )

""" selekcia podla gaussoveho normalneho rozlozenia """
def gauss_selection(list_population):
	gk = 68 # percent najlepsich vysledkov vybrat
	n = round(len(list_population * gk) / 100)
	#todo
	new_population = []
	for i in range(0, int(n)):
		item = find_best_fitness(list_population)
		if item:
			list_population.remove(item)
			new_population.append(item)
	return new_population

""" vyber najlepsieho rodica z populacie """
def best_parent_selection(list_population):
	return find_best_fitness(list_population)

""" jednobodove krizenie 2 chromozomov """
def one_point_crosover(chromo_a, chromo_b):
	new_population = []	
	chromo_a1 = chromo_a[0:(len(chromo_a) / 2)] +chromo_b[len(chromo_b) / 2:len(chromo_b)]
	chromo_b1 = chromo_b[0:(len(chromo_b) / 2)] +chromo_a[len(chromo_a) / 2:len(chromo_a)]
	new_population.append(chromo_a1)
	new_population.append(chromo_b1)
	return new_population

""" mutacia chromozomu """
def mutation(chromo):
	if chromo:
		id = random.randint(0,len(chromo)-1)
		chromo[id] = False if chromo[id] else True
	return chromo 

""" najdenie najlepsieho druhu z populacie """
def find_best_fitness(list_population):	
	item = None
	if len(list_population)>0:
		f = list_population[0]
		for i in list_population:
			if f < get_fitness(i):
				item = i
				f = get_fitness(i)
	return item

""" doplnenie chybajucich druhov populacie do celkoveho poctu """
def fill_populations(list_population, max):
	idx = 0
	new = []
	for i in range(len(list_population), max):
		if idx < len(list_population):
			new.append(mutation(list_population[idx]))
			idx += 1
	list_population.extend(new)
	return list_population

# main ------------------------------------------------------

# init input values
count = raw_input("Count of generated population [100]: ")
chromo_n = raw_input("Count of bits of chromozome [8]: ")
populations = raw_input("Count of populations [10]: ")
debug = raw_input("Debug mode y/n [y]: ")
try:
	list_population = []
	if count == "":
		count = 100
	if chromo_n == "":
		chromo_n = 8
	if populations == "":
		populations = 10
	if debug == "" or debug =="y":
		debug = True
	else:
		debug = False

	# init first generation
	for i in range(0, int(count)):
		list_population.append(bitarray(generate_chromozone(chromo_n)))
except ValueError:
	print('Error input! \nInput has to be integer')
	exit()		

# start 
print('\n0.generation')

idx = 1;
while int(populations)>idx:
	selected_list = gauss_selection(list_population)
	if debug:
		print('\nAfter gauss selection')
	
	show(selected_list)
	crosover_list = []
	for i in range(0, len(selected_list)-1):
		if i % 2 == 0:
			cs = one_point_crosover(selected_list[i], selected_list[i+1])
			if cs[0] not in crosover_list:
				crosover_list.append(cs[0])
			if cs[1] not in crosover_list:
				crosover_list.append(cs[1])
	
	if random.randint(0,4)==1:
		if debug:
			print('\nParent devolution') # 20 percentna sanca ze sa rodic prenesie
		best = best_parent_selection(selected_list)
		if best not in crosover_list:  
			crosover_list.append(best)
	
	if random.randint(0,9)==1:
		if debug:
			print('\nParent mutation') # 10 percentna sanca ze nastane mutacia
		best = best_parent_selection(selected_list)
		mut = mutation(best)
		if mut not in crosover_list:  
			crosover_list.append(mut)

	list_population = fill_populations(crosover_list, int(count)) #crosover_list

	print('\n'+ str(idx) +'.generation')
	idx = idx +1

print('\nThe best final chromozone in population')
best = best_parent_selection(list_population)

show([best])



